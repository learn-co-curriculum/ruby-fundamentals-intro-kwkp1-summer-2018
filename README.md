# You will be able to:

1. Explain what a **program** is
2. Explain what **strings**, **integers**, and **floats** are and how to manipulate them
3. Explain what **puts** is and what it's used for
4. Explain what a **variable** is and how to use it to store data
5. Use the **gets** method to take in user input
6. **Predict errors** in your code

# Welcome to Ruby!

Ruby is a simple, yet powerful programming language that you can use for everything from web development to processing text to building games. It's what is known as a **backend programming language**. Backend programming languages handle all the behind the scenes "magic" of a website.

Let's take Yelp.com, for example. Say you want to search for a sushi restaurant in the East Village. You'd type 'East Village' and 'sushi' into the form on the site. Once you press submit, the application checks your criteria against a database. The database is like a ginormous Excel spreadsheet that stores all the information about the thousands of businesses you can find on Yelp. Once the database finds the information that matches your search, it'll display the results back to you on the website. You don't actually see any of this database stuff happen, so that's why it seems like magic. From logging you in to your Facebook account, to actually sending emails from your Gmail account, to posting a tweet on your feed, a backend programming language takes care of everything that happens in a web application that you don't see.

Some sites built using Ruby: [GitHub](http://github.com), [Groupon](http://groupon.com), [Airbnb](http://airbnb.com), [SoundCloud](http://soundcloud.com), [Square](http://square.com) and the all apps you'll build in this course!
